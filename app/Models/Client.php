<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    use HasFactory;

    protected $fillable = [
        'IdTypeDocument',
        'Document',
        'Name',
        'Email',
        'Phone',
        'Nationality',
    ];

    public function TypeDocument()
    {
        return $this->hasOne(TypeDocument::class, 'id', 'IdTypeDocument');
    }
}
