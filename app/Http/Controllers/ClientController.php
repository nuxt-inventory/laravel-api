<?php

namespace App\Http\Controllers;

use App\Models\Client;
use Illuminate\Http\Request;

class ClientController extends Controller
{
    public function index()
    {
        return response()->json([
            'error' => false,
            'message' => "Listado Obtenido con exito",
            'data' => Client::with('TypeDocument')->get()
        ]);
    }

    public function show(Client $client)
    {
        return response()->json([
            'error' => false,
            'message' => "Cliente Obtenido con exito",
            'data' => $client
        ]);
    }

    public function store(Request $request)
    {
        $client = Client::create([
            'IdTypeDocument' => $request->IdTypeDocument,
            'Document' => $request->Document,
            'Name' => strtoupper($request->Name),
            'Email' => strtoupper($request->Email),
            'Phone' => strtoupper($request->Phone),
            'Nationality' => strtoupper($request->Nationality),
        ]);

        return response()->json([
            'error' => false,
            'message' => "Cliente creado con exito",
            'data' => $client
        ]);
    }

    public function update(Request $request, Client $client)
    {
        $client->IdTypeDocument = $request->IdTypeDocument;
        $client->Document = $request->Document;
        $client->Name = strtoupper($request->Name);
        $client->Email = strtoupper($request->Email);
        $client->Phone = $request->Phone;
        $client->Nationality = strtoupper($request->Nationality);
        $client->save();

        return response()->json([
            'error' => false,
            'message' => "Cliente actualizado con exito",
            'data' => $client
        ]);
    }

    public function status($status, Client $client)
    {
        $client->Status = $status;
        $client->save();

        return response()->json([
            'error' => false,
            'message' => "Actualizacion de estado con exito",
            'data' => $client
        ]);
    }

}
