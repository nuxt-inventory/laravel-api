<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\Provider;

class ProviderController extends Controller
{
    public function index()
    {
        return response()->json([
            'error' => false,
            'message' => "Listado Obtenido con exito",
            'data' => Provider::with('TypeDocument')->get()
        ]);
    }

    public function show(Provider $provider)
    {
        return response()->json([
            'error' => false,
            'message' => "Proveedor Obtenido con exito",
            'data' => $provider
        ]);
    }

    public function store(Request $request)
    {
        $provider = Provider::create([
            'IdTypeDocument' => $request->IdTypeDocument,
            'Document' => $request->Document,
            'Name' => strtoupper($request->Name),
            'Email' => strtoupper($request->Email),
            'Phone' => strtoupper($request->Phone),
            'Nationality' => strtoupper($request->Nationality),
        ]);

        return response()->json([
            'error' => false,
            'message' => "Proveedor creado con exito",
            'data' => $provider
        ]);
    }

    public function update(Request $request, Provider $provider)
    {
        $provider->IdTypeDocument = $request->IdTypeDocument;
        $provider->Document = $request->Document;
        $provider->Name = strtoupper($request->Name);
        $provider->Email = strtoupper($request->Email);
        $provider->Phone = $request->Phone;
        $provider->Nationality = strtoupper($request->Nationality);
        $provider->save();

        return response()->json([
            'error' => false,
            'message' => "Proveedor actualizado con exito",
            'data' => $provider
        ]);
    }

    public function status($status, Provider $provider)
    {
        $provider->Status = $status;
        $provider->save();

        return response()->json([
            'error' => false,
            'message' => "Actualizacion de estado con exito",
            'data' => $provider
        ]);
    }
}
