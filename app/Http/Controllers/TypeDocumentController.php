<?php

namespace App\Http\Controllers;

use App\Models\TypeDocument;
use Illuminate\Http\Request;

class TypeDocumentController extends Controller
{
    public function index()
    {
        return response()->json([
            'error' => false,
            'message' => 'Registros Mostrados con exito',
            'data' => TypeDocument::all()
        ]);
    }
    public function store(Request $request)
    {
        $type = TypeDocument::create([
            'type' => strtoupper($request->type)
        ]);

        return response()->json([
            'error' => false,
            'message' => 'Registro Creado con exito',
            'data' => $type
        ]);
    }

    public function show(TypeDocument $typeDocument)
    {
        return response()->json([
            'error' => false,
            'message' => 'Registro Mostrado con exito',
            'data' => $typeDocument
        ]);
    }
    public function update(Request $request, TypeDocument $typeDocument)
    {
        $typeDocument->type = strtoupper($request->type);
        $typeDocument->save();

        return response()->json([
            'error' => false,
            'message' => 'Registro Actualziado con exito',
            'data' => $typeDocument
        ]);
    }

    public function destroy(TypeDocument $typeDocument)
    {
        $typeDocument->delete();

        return response()->json([
            'error' => false,
            'message' => 'Registro Actualziado con exito',
            'data' => $typeDocument
        ]);
    }
}
