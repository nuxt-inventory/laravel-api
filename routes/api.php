<?php

use App\Http\Controllers\{ClientController, ProviderController, TypeDocumentController};
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});


Route::resource('clients', ClientController::class);
Route::resource('type-documents', TypeDocumentController::class);
Route::resource('providers', ProviderController::class);

Route::get('clients/status/{status}/{client}', [ClientController::class, 'status']);
Route::get('providers/status/{status}/{provider}', [ProviderController::class, 'status']);
